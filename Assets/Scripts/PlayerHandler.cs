﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerHandler : MonoBehaviour
{
	[Header("General")]
	[Tooltip("In ms^-1")] [SerializeField] float controlSpeed = 20f;
	[Tooltip("In ms")] [SerializeField] float xRange = 5.5f;
	[Tooltip("In ms")] [SerializeField] float yRange = 3f;

    [SerializeField] GameObject[] guns;

	[Header("Screen-position Based")]
	[SerializeField] float positionPitchFactor = -5f;
	[SerializeField] float positionYawFactor = 5f;


	[Header("Control-throw Based")]
	[SerializeField] float controlRollFactor = -20f;
	[SerializeField] float controlPitchFactor = -20f;

	float xThrow, yThrow;
    bool isControlEnabled = true;


	void Update()
	{
        if (isControlEnabled)
        {
            ProcessTranslate();
            ProcessRotation();
            ProcessFiring();
        }
	}

    void OnPlayerDeath ()//called by string reference 
    {
        isControlEnabled = false;
        print("Controls frozen");
    }

	private void ProcessTranslate()
	{
		xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
		yThrow = CrossPlatformInputManager.GetAxis("Vertical");

		float xOffset = xThrow * controlSpeed * Time.deltaTime;
		float yOffset = yThrow * controlSpeed * Time.deltaTime;

		float rawXPos = transform.localPosition.x + xOffset;
		float clampedXPos = Mathf.Clamp(rawXPos, -xRange, xRange);// define xRange as a Serialized Field

		float rawYPos = transform.localPosition.y + yOffset;
		float clampedYPos = Mathf.Clamp(rawYPos, -yRange, yRange);// define yRange as a Serialized Field

		transform.localPosition = new Vector3(clampedXPos, clampedYPos, transform.localPosition.z);//only it will move on the x and y axis
	}
	private void ProcessRotation()
	{
		float pitchDueToPosition = transform.localPosition.y * positionPitchFactor;
		float pitchDueToControlThrow = yThrow * controlPitchFactor;// pitch   since yThrow  and xThrow was not member variable it was defined on the top, add yThrow to couple it with the throw
		float pitch = pitchDueToPosition + pitchDueToControlThrow;//

		float yawDueToPosition = transform.localPosition.x * positionYawFactor;//yaw depends on position

		float rollDueToControlThrow = xThrow * controlRollFactor;//roll depends on control throw

		transform.localRotation = Quaternion.Euler(pitch, yawDueToPosition, rollDueToControlThrow);
	}
    private void ProcessFiring()
    {
        if (CrossPlatformInputManager.GetButton("Fire"))
            {
            //print("Firing");
            SetGunsActive(true);

            }
        else

            {
            SetGunsActive(false);

            }
    }

    private void SetGunsActive(bool isActive)
    {
        foreach (GameObject gun in guns)//care my affect death FX
        {
            var emissionModule = gun.GetComponent<ParticleSystem>().emission;//get to the emission part of the particle system of each gun, and store it in a variable(this is done to enable and disable the emission of each gun without deactivating the whole object
            emissionModule.enabled = isActive;
        }
    }




}
