﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    [SerializeField] GameObject deathFX;
    [SerializeField] Transform parent;
    [SerializeField] int scorePerHit = 12;
    [SerializeField] int hits = 10;



    ScoreBoard scoreBoard;

    // Start is called before the first frame update
    void Start()
    {
        AddBoxCollider();
        scoreBoard = FindObjectOfType<ScoreBoard>();//look at runtime in the game each of the enemies will find a reference down to the scoreboard script
    }

    private void AddBoxCollider()
    {
        Collider boxCollider = gameObject.AddComponent<BoxCollider>();//store the box collider time in a variable called boxCollider
        boxCollider.isTrigger = false;
    }

    private void OnParticleCollision(GameObject other)
    {
        NewMethod();

        if (hits <= 1)
        {
            KillEnemy();

        }
        //print("Particles collided with enemy"+ gameObject.name);


    }

    private void NewMethod()
    {
        scoreBoard.ScoreHit(scorePerHit);//this will increase the score for the default stated in  on the scorePerhit
        hits = hits - 1;
    }

    private void KillEnemy()
    {
        GameObject fx = Instantiate(deathFX, transform.position, Quaternion.identity);
        fx.transform.parent = parent;
        Destroy(gameObject);
    }


}
