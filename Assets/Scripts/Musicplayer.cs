﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Musicplayer : MonoBehaviour
{

    private void Awake()
    {
        int numMusicPlayers = FindObjectsOfType<Musicplayer>().Length;//get all the music player in the scene 

        //print("Number of music players in the scene" + numMusicPlayers);

        //if more than one music player in scene
        //destroy ourselves
        //else

        if (numMusicPlayers > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);//gameObject is a variable 

        }

    }
}   
    

