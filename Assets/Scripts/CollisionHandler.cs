﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;// ok as long as this is the only script that load scenes

public class CollisionHandler : MonoBehaviour
{

    [Tooltip("In seconds")][SerializeField] float levelLoadDelay = 1f;
    [Tooltip ("FX prefab on player")] [SerializeField] GameObject deathFX;


    void OnTriggerEnter(Collider other)//collides with something, which means the objects apply a force to each other. A trigger is just a collider set only to report overlaps rather than apply a force.
    {
        StartDeathSequence();
        deathFX.SetActive(true);
        Invoke("ReloadScene",levelLoadDelay);
    }

    private void StartDeathSequence()
    {
        SendMessage("OnPlayerDeath");

    }

    private void ReloadScene()//string referenced
    {
        SceneManager.LoadScene(1);
    }
}
